
require('dotenv').config();

export default {
  mode: 'universal',
  srcDir: 'src/',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/pwa',
  ],
  env: {
    baseUrl: process.env.BASE_URL || '',
    apiBaseUrl: process.env.API_BASE_URL || '',
    firebaseConfig: {
      apiKey: process.env.FIREBASE_CONFIG_API_KEY || '',
      authDomain: process.env.FIREBASE_CONFIG_AUTH_DOMAIN || '',
      databaseURL: process.env.FIREBASE_CONFIG_DATABASE_URL || '',
      projectId: process.env.FIREBASE_CONFIG_PROJECT_ID || '',
      storageBucket: process.env.FIREBASE_CONFIG_STORAGE_BUCKET || '',
      messagingSenderId: process.env.FIREBASE_CONFIG_MESSAGING_SENDER_ID || '',
    },
  },
  /*
  ** Build configuration
  */
 buildDir: process.env.BUILD_DIR || './functions/.nuxt',
 build: {
   /*
   ** You can extend webpack config here
   */
   analyze: process.env.ENABLE_ANALYZE_MODE === 'true',
   publicPath: '/assets/',
   extractCSS: true,
   extend (config, ctx) {
   }
 }
}
